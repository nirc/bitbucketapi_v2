const enums = require('./enums');

function api (client, basePath) {
    this.client = client;
    this.basePath = basePath;

    // get all repositores
    var all = function (repository) {
        return this.client.apiCall({path: [this.basePath, repository, 'commits'].join('/')});
    }.bind(this);

    let branch = function (repository, branch) {
        return this.client.apiCall({path: [this.basePath, repository, 'commits', branch].join('/')});
    }.bind(this);

    return {
        all: all,
        branch: branch
    };
}

module.exports = api;