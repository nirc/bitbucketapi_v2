const BitBucketApi = require('bitbucketapi');
const _ = require('lodash');
// inner components
const Repositories = require('./repositories');
const PullRequests = require('./pull-requests');

/**
 * 
 * @param {object} options
 * @param {string} options.token - Bitbucket oauth access token
 * @param {string} options.user - Bitbucket user name
 * @optional
 * 
 * @constructor
 */
let API = function (options) {
    _.extend({}, options);

    if (!options.token) {
        // TODO : throw error must param
    }

    if (!options.user) {
        // TODO : output warning, no user is defind, will use bitbucket public account
    }

    // init client
    this.bitbucket = new BitBucketApi(options);
    this.user = options.user;

    let repositories = new Repositories(this.bitbucket, this.user);
    let pullRequests = new PullRequests(this.bitbucket, this.user);
    
    return {
        // raw client
        api: this.bitbucket,
        // additional methods
        Repositories: repositories,
        PullRequests: pullRequests
    };
};

API.prototype.repositories = Repositories;

module.exports = API;