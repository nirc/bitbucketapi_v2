const enums = require('./enums');

function api (client, user) {
    this.client = client;
    this.user = user;

    var createURL = function (repository) {
        return ['/repositories', this.user, repository, 'pullrequests'].join('/');
    }.bind(this);

    // get all repositores
    var all = function (repository) {
        return this.client.apiCall({path: createURL(repository)});
    }.bind(this);

    var getPullRequst = function (repository, pullRequestID) {
        return this.client.apiCall({path: createURL(repository) + '/' + pullRequestID});
    }.bind(this);

    /**
     * this action is bound to the user
     * @type {function(this:api)}
     */
    var approve = function (repository, pullRequestID) {
        return this.client.apiCall({
            path: createURL(repository) + '/' + pullRequestID + '/approve',
            method: enums.RequestType.POST
        });
    }.bind(this);

    /**
     * this action is bound to the user
     * @type {function(this:api)}
     */
    var unapprove = function (repository, pullRequestID) {
        return this.client.apiCall({
            path: createURL(repository) + '/' + pullRequestID + '/approve',
            method: enums.RequestType.DELETE
        });
    }.bind(this);

    var decline = function (repository, pullRequestID) {
        return this.client.apiCall({
            path: createURL(repository) + '/' + pullRequestID + '/decline',
            method: enums.RequestType.POST
        });
    }.bind(this);

    var merge = function (repository, pullRequestID) {
        return this.client.apiCall({
            path: createURL(repository) + '/' + pullRequestID + '/merge',
            method: enums.RequestType.POST
        });
    }.bind(this);

    /**
     * create is taken from:
     * https://bitbucket.org/site/master/issues/8195/rest-api-for-creating-pull-requests
     * the official doc is missing for this function, also the official response in the issue doesn't seem to work
     * the only thing that seems to work is sending the raw JSON (last response by Dan Shumaker).
     *
     * @method create
     * @param {string} repository
     * @param {object} options
     * @param {string} options.source - the source branch
     * @param {string} options.title - the pull request title
     * @param {string} options.target - the target branch
     * @optional
     * @param {string} options.description - the pull request description
     * @optional
     * @type {function(this:api)}
     */
    var create = function (repository, options) {
        var options = options || {};
        let payload = {
            source: {
                branch: {
                    name: options.source
                }
            },
            title: options.title
        };
        // add optional parameters
        options.dest ? payload.dest = options.dest : null;
        options.description ? payload.description = options.description : null;

        return this.client.apiCall({
            path: createURL(repository),
            method: enums.RequestType.POST,
            payload: payload
        });
    }.bind(this);

    return {
        all: all,
        getPullRequst: getPullRequst,

        create: create,
        decline: decline,
        merge: merge,

        approve: approve,
        unapprove: unapprove
    };
}

module.exports = api;