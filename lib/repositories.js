const enums = require('./enums');
const Commits = require('./repositories-commits');

function api (client, user) {
    this.client = client;
    this.basePath = '/repositories';

    if (user) {
        this.basePath += '/' + user;
    }

    // get all repositores
    var all = function () {
        return this.client.apiCall({path: this.basePath});
    }.bind(this);

    var getRepository = function (repository) {
        return this.client.apiCall({path: this.basePath + '/' + repository});
    }.bind(this);

    let commits = new Commits(client, this.basePath);

    return {
        Commits: commits,

        all: all,
        getRepository: getRepository
    };
}

module.exports = api;