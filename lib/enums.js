const enums = {
    RequestType: {
        GET: 'GET',
        POST: 'POST',
        DELETE: 'DELETE',
        PUT: 'PUT'
    }
};

module.exports = enums;